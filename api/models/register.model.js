const mongoose = require('mongoose');

const registerSchema = new mongoose.Schema({
    username: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    emailId: {
        type: String,
        require: true
    },
    mobileNo: {
        type: String,
        default: null
    }
});

const registerModel = mongoose.model('Register', registerSchema);

module.exports = registerModel;
