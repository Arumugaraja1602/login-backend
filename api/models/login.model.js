const mongoose = require('mongoose');

const loginSchema = new mongoose.Schema({
    emailId:{
        type: String,
        require: true
    },
    password:{
        type: String,
        require: true
    }
});

const loginModel = mongoose.model('Login', loginSchema);

module.exports = loginModel;