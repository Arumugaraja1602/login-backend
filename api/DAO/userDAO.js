const registerModel = require('../models/register.model');

const getUsers = (object) =>{
    new Promise((resolve, reject)=>{
        registerModel.find(object)
        .then((data)=> resolve(data))
        .catch((err)=> reject(err));
    });
}

const createUser = (object) =>{
    new Promise((resolve, reject)=>{
       new registerModel(object)
        .save()
        .then((data)=> resolve(data))
        .catch((err)=> reject(err));
    });
}

// module.exports = getUsers;
// module.exports = createUser;

module.exports = {
    getUsers: getUsers,
    createUser: createUser
}