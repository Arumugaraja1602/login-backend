const express = require('express');
const authUser = require('../auth/authUser');
const router = express.Router();
const loginModel = require('../models/login.model');

router.get('/getAll', (req, res)=>{
   loginModel.find((err, data)=>{
    if(err){
        res.send({status: 500, message: 'Unable to get Users'});
    }
    else{
        // res.send({status:200, message:'success', user: data});
        res.json(data);
    }
   });
});

router.post('/create', authUser.login);

// router.post('/create', (req, res)=>{
    
//     let emailId = req.body.emailId;
//     let password = req.body.password;

//     let loginObj = new loginModel({emailId, password});

//     loginObj.save((err, loginObj)=>{
//         if(err){
//             res.send({status: 500, message: 'Unable to Login user'});
//         }
//         else{
//             // res.send({status: 200, message: 'Login Created Successfully', user: loginObj});
//             res.json(loginObj);
//         }
//     });
// });

module.exports = router;