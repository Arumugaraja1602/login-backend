const express = require('express');
const router = express.Router();
const registerModel = require('../models/register.model');
const authUser = require('../auth/authUser');

router.get('/getAll', (req, res)=>{
    registerModel.find((err, data)=>{
        if(err){
            res.send({status: 500, message: 'Unable to get Users'});
        }
        else{
            // res.send({status: 200, message: 'success' ,user: data});
            res.json(data);
        }
    });
});

router.post('/create', authUser.register);




// router.post('/create', (req, res)=>{
    
//     let registerObj = new registerModel(req.body);
//     console.log('registerObj:', registerObj);
//     registerObj.save((err, registerObj)=>{
//         if(err){
//             res.send({status: 500, message: 'User Register is not successful'});
//         }
//         else{
//             // res.send({status: 200, message: 'User Register Successfully', user: registerObj});
//             res.json(registerObj);
//             // console.log('registerObj:', registerObj);
//         }
//     });
// });

// router.route('/create').post(async (req, res)=>{
//     if(!req.body.username || !req.body.password || !req.body.emailId){
//         res.send({status:401, message: 'User details missing'});
//     } else{
//         try{
//             let emailId = req.body.emailId;
//             new Promise((resolve, reject)=>{
//                 registerModel.find({emailId});
//             }).then((data)=> {
//                 console.log('data:', data);
                
//                 if(data){
//                     res.send({message: 'EmailId already registered'});
//                 }
//                 res.send({status:200, message:'Registered Successfully'});
//             })
            
//         } catch(err){
//             res.send({status:404, message:'Something went wrong', error: err});
//         }
//     }
// })


module.exports = router;