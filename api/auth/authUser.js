const jwt = require('jsonwebtoken');
const userDAO = require('../DAO/userDAO');
const bcrypt = require('bcrypt');
const registerModel = require('../models/register.model');

const JWT_SECRET = 'oh9mavuaje9y940325o326y8372590&*^*U#)$(*#&^&*@*N*AHO*OIJ9w352';

let register = async (req, res) => {
    if (!req.body.emailId || !req.body.password) {
        res.send({ status:401, message: 'Parameters are missing' });
    } else {
        try {
            let object = {
                emailId: req.body.emailId
            }
            // const checkEmail = await userDAO.getUsers(object);
            const checkEmail = await registerModel.find(object);
            if (checkEmail.length > 0) {
                res.send({ message: 'Email Id already exists' });
            } else {
                let userData = {
                    username: req.body.username,
                    password: await bcrypt.hash(req.body.password, 10),
                    emailId: req.body.emailId,
                    mobileNo: req.body.mobileNo
                }
                // const addUser = await userDAO.createUser(userData);
                const addUser = new registerModel(userData).save();
                if (addUser) {
                    res.send({ status: 200, message: 'User Register Successfully' });
                } else {
                    res.send({ status: 403, message: 'Something went wrong' });
                }
            }
        } catch (err) {
            res.send({ status: 404, message: 'Something went wrong', Error: err });
        }
    }
}

let login = async (req, res) => {
    if(!req.body.emailId || !req.body.password){
        res.send({status:401, message:'Parameters missing'});
    } else {
        try{
            let object = {
                emailId: req.body.emailId
            }
            const user = await registerModel.find(object);
            if(user.length > 0){
                const validPassword = await bcrypt.compare(req.body.password, user[0].password)
                if(validPassword){
                    const token = jwt.sign(
                        {
                            id: user[0]._id,
                            username: user[0].username
                        },
                        JWT_SECRET
                    )
                    res.send({status:200, message:'Authorized user', token: token});
                } else {
                    res.send({status:401, message: 'Invalid Password', token: null});
                }
            } else{
                res.send({status:401, message:'Unauthorized user!! Register First', token: null});
            }
        } catch(err){
            res.send({status:403, message:'Something went wrong'});
        }
    }
}



module.exports = {
    register: register,
    login: login
}