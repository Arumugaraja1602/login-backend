const express = require('express');
const app = express();
const mongoose = require('mongoose');

// Port connection
const port = 5000;
app.listen(port, () => {
    console.log(`Connected to the Port: ${port} Successfully`)
})

// Mongodb connection
mongoose.connect('mongodb://localhost:27017/dinamic',
    { useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => {
        console.log('Database connected Successfully');
    })
    .catch((e) => {
        console.log(`Database Connection Error: ${e.message}`)
    })


app.use(express.urlencoded());
app.use(express.json());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const loginRouter = require('./api/routes/login.api');
const registerRouter = require('./api/routes/register.api');

app.use('/login', loginRouter);
app.use('/register', registerRouter);











/*
// read data from node
app.get('/getRegister', (req, res) => {
    fs.readFile('database.json', (err, data) => {
        if (err) {
            throw err;
        }
        let user = JSON.parse(data);
        console.log(user);
    });
    res.send('data getting from server');
}); */

/*
// testing 1
app.post('/createRegister', (req, res) => {
    let data = req.body;
    let object = JSON.stringify(data, null, 2);
    // fs.writeFile('database.json', object, (err) => {
    //     if (err) {
    //         throw err
    //     }
    //     console.log('Data added to file');
    // });
    fs.readFile('database.json', (err, data) => {
        if (err) {
            throw err;
        }
        const user = JSON.parse(data);
        let newUser = user.replace(']', ',')
        console.log(newUser);
        fs.writeFile('database.json', newUser, (err) => {
            if (err) throw err;
        })
        // const log = fs.createWriteStream('database.json', {flags: 'a'});
        // log.write(',' + object + ' \n');
        // log.end();
    })
    res.send('created successfully');
}) */

/*
// testing 2
var dummy = {
    "username": "kathir",
    "password": "test",
    "emaidId": "raja@gmail.com",
    "mobileNo": 788951482196
}

fs.readFile('database.json', 'utf8', function (err, data) {

    var newValue = data.replace(']', ',');
    const log = fs.createWriteStream('database.json', {flags: 'a'})
    newValue = log.write(' \n' + ',' + JSON.stringify(dummy, null, 2) + ']');
    console.log('new value: ', newValue);
    fs.writeFile('database.json', newValue, function () {
    });
});
console.log('You have modified the file ');
*/




